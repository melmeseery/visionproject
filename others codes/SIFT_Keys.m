function Keypoint = SIFT_Keys(image)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   identify the feature descriptors 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% convert image to gray scale [form 0 to 1]
image = mat2gray(image);
% size of teh image
[ix,iy] = size(image);
%initial sigma for gaussian window
iSigma = 1;
%gaussian window
gWin = fspecial('gaussian',[9 9],iSigma);  
%filter image with window
image=imfilter(image,gWin);
%save results in Keypoint
Keypoint = [];
% the number of scales
RunNum = 0;
% scales of the DOG
ScaDoG = zeros(6,1);

%min size of the image is 20 pixels
while (ix>20) & (iy>20), 
    % analyse 6 scales of the image
    IMG = zeros(ix,iy,6);
    % we have 5 DOG
    DoG = zeros(ix,iy,5);
    %current image
    IMG(:,:,1) = image;
    %current scale
    ScaDoG(1) = iSigma*2^RunNum;
    for i=1:5
        %the next scale is divided by 2
        Sigma = 2^(i/5);
        % gaussian window
        gWin = fspecial('gaussian',[9 9],Sigma);
        %filter image
        IMG(:,:,i+1) = imfilter(image,gWin);
        % save scale
        ScaDoG(i+1) = Sigma*ScaDoG(1);
    end
    % evaluate the DOG
    for i = 1:5;
        DoG(:,:,i) = IMG(:,:,i+1)-IMG(:,:,i);
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % identify the keypoint
    % loc maxima and minima are from [k-1 k k+1] images
    for k = 2:4,
       %temporal DOG
       DoGTemp = DoG(:,:,k-1:k+1);
       % loc maxima
       m1=max(DoGTemp(3:end-2,3:end-2,:),[],3);
       %loc minima
       m2=min(DoGTemp(3:end-2,3:end-2,:),[],3);
       % find indices of maxima and mininima
       clma=max(im2col(m1,[3 3],'sliding'));
       clmi=min(im2col(m2,[3 3],'sliding'));
       cl2=DoG(4:end-3,4:end-3,k);
       %linear indices
       ind=find((clma==cl2(:)')|(clmi==cl2(:)'));
       tind=find(cl2(ind)~=0);
       ind=ind(tind);
       % find multiple subscripts from linear index
       [ii,jj]=ind2sub([ix-6 iy-6],ind);
       ii=ii+3;
       jj=jj+3;
       
       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       % analyse all maxima and minima
       for kk=1:numel(ind),
           % current coordinate on the image
           i=ii(kk);
           j=jj(kk);
           %analyse local point at DOG and its surround  
           Dxx = DoG(i+1,j,k)+DoG(i-1,j,k)-2*DoG(i,j,k);
           Dyy = DoG(i,j+1,k)+DoG(i,j-1,k)-2*DoG(i,j,k);
           Dxy = DoG(i+1,j+1,k)+DoG(i,j,k)-DoG(i+1,j,k)-DoG(i,j+1,k);                       
           r = (Dxx+Dyy)^2/(Dxx*Dyy-Dxy^2);
           %if we have good contrast in this point - estimate its keypoint
           if r<10,
               Dx = DoG(i+1,j,k)-DoG(i,j,k);
               Dy = DoG(i,j+1,k)-DoG(i,j,k);
               Dz = DoG(i,j,k+1)-DoG(i,j,k);
               Dxz = DoG(i+1,j,k+1)+DoG(i,j,k)-DoG(i+1,j,k)-DoG(i,j,k+1);
               Dyz = DoG(i,j+1,k+1)+DoG(i,j,k)-DoG(i,j+1,k)-DoG(i,j,k+1);
               Dzz = DoG(i,j,k+1)+DoG(i,j,k-1)-2*DoG(i,j,k);
               xhTemp = [Dxx Dxy Dxz;Dxy Dyy Dyz;Dxz Dyz Dzz];
               xHead = pinv(xhTemp)*[Dx Dy Dz]';
               % estimate keypoint (coordinates and scale, gradient magnitude and orientation)
               if abs(xHead(1))<1 & abs(xHead(2))<1 & abs(xHead(3))<0.5
                   key = [i+xHead(1),j+xHead(2),ScaDoG(k)+xHead(3),0,0,0];
                   Keypoint(end+1,:) = descriptor(key,IMG(:,:,k),RunNum);
               end
           end
       end
    end 
    % resize image
    image = image(1:2:end,1:2:end);
    %new image size
    [ix,iy] = size(image);
    % next run
    RunNum = RunNum +1;
end  

% estimate keypoint (gradient magnitude and orientation)
function key = descriptor(key,IM,RunNum)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%current coordinates
x = fix(key(1));
y = fix(key(2));
%gradient magnitude
key(4) = sqrt((IM(x+1,y)-IM(x-1,y))^2+(IM(x,y+1)-IM(x,y-1))^2);
%gradient orientation
key(6) = atan2(IM(x,y+1)-IM(x,y-1),IM(x+1,y)-IM(x-1,y));

% main orientation
Orient = zeros(1,4);
%local grid
[M,N]=ndgrid(x-1:x+1,y-1:y+1);
%local magnitude and orientation
theta = atan2(IM(M,N+1)-IM(M,N-1),IM(M+1,N)-IM(M-1,N));
thetaT = fix((theta + pi)/(pi/4)); 
% extimate orientations (2 directions  - positiv and negative directions)
ind=find((thetaT==0)|(thetaT==7));
Orient(1)=numel(ind);

ind=find((thetaT==1)|(thetaT==2));
Orient(2)=numel(ind);

ind=find((thetaT==3)|(thetaT==4));
Orient(3)=numel(ind);

ind=find((thetaT==5)|(thetaT==6));
Orient(4)=numel(ind);
%find max orientation
[x,OriI] = max(Orient);
% save orientation
key(5) = OriI; %  
%adjust coorinates to one scale, each image is resized
key(1) = key(1)*(2^RunNum);
key(2) = key(2)*(2^RunNum);    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%